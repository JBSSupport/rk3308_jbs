// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2018 Fuzhou Rockchip Electronics Co., Ltd
 */

#include <dt-bindings/input/input.h>
#include "arm64/rockchip/rk3308-pcba.dtsi"

/ {
	model = "Rockchip RK3308 Ali Module Board V10";
	compatible = "rockchip,rk3308-ali-module-v10", "rockchip,rk3308";

	chosen {
		bootargs = "earlyprintk earlycon=uart8250,mmio32,0xff0c0000 swiotlb=1 console=ttyFIQ0 root=PARTUUID=614e0000-0000 rootwait";
	};

	acodec_sound: acodec-sound {
		compatible = "rockchip,multicodecs-card";
		rockchip,card-name = "rockchip,rk3308-acodec";
		/* rockchip,codec-hp-det; */
		rockchip,mclk-fs = <256>;
		rockchip,cpu = <&i2s_8ch_2>;
		rockchip,codec = <&acodec>;
		status = "okay";
	};

	grf1: grf1@ff500000 {
		compatible = "rockchip,rk3308-grf", "syscon", "simple-mfd";
		reg = <0x0 0xff500000 0x0 0x10000>;
	};

	adc-keys {
		compatible = "adc-keys";
		io-channels = <&saradc 1>;
		io-channel-names = "buttons";
		poll-interval = <100>;
		keyup-threshold-microvolt = <1800000>;

		recovery-key {
			linux,code = <KEY_F1>;
			label = "key_f1";
			press-threshold-microvolt = <1440000>;
		};
		mode-key {
			linux,code = <KEY_MODE>;
			label = "mode";
			press-threshold-microvolt = <900000>;
		};
	};
	
	adc1-keys {
		compatible = "adc1-keys";
		io-channels = <&saradc 4>;
		io-channel-names = "buttons";
		poll-interval = <500>;
		keyup-threshold-microvolt = <1800000>;

		recovery-key {
			linux,code = <KEY_F2>;
			label = "key_f2";
			press-threshold-microvolt = <1440000>;
		};
	};

	dummy_codec: dummy-codec {
		compatible = "rockchip,dummy-codec";
		#sound-dai-cells = <0>;
	};
	/*
	gpio-keys {
		compatible = "gpio-keys";
		autorepeat;

		pinctrl-names = "default";
        pinctrl-0 = <&volumeup
            &volumedown
            &mute>;

		volumeup {
			gpios = <&gpio0 RK_PA7 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEUP>;
            label = "GPIO Key Volumeup";
			wakeup-source;
			debounce-interval = <100>;
		};

		volumedown {
			gpios = <&gpio0 RK_PA6 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_VOLUMEDOWN>;
			label = "GPIO Key Volumedown";
			wakeup-source;
			debounce-interval = <100>;
		};

		mute {
			gpios = <&gpio0 RK_PC4 GPIO_ACTIVE_LOW>;
			linux,code = <KEY_MODE>;
			label = "GPIO Key mode";
			wakeup-source;
			debounce-interval = <100>;
		};
	};
	*/
	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio0 RK_PA2 GPIO_ACTIVE_LOW>;
	};

	spdif_tx_sound: spdif-tx-sound {
		status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,name = "rockchip,spdif-tx-sound";
		simple-audio-card,cpu {
			sound-dai = <&spdif_tx>;
		};
		simple-audio-card,codec {
			sound-dai = <&dummy_codec>;
		};
	};

	vdd_core: vdd-core {
		compatible = "pwm-regulator";
		pwms = <&pwm0 0 5000 1>;
		regulator-name = "vdd_core";
		regulator-min-microvolt = <827000>;
		regulator-max-microvolt = <1340000>;
		regulator-init-microvolt = <1015000>;
		regulator-always-on;
		regulator-boot-on;
		regulator-settling-time-up-us = <250>;
		status = "okay";
	};

        vdd_log: vdd-log {
                compatible = "regulator-fixed";
                regulator-name = "vdd_log";
                regulator-always-on;
                regulator-boot-on;
                regulator-min-microvolt = <1050000>;
                regulator-max-microvolt = <1050000>;
        };

	vdd_1v0: vdd-1v0 {
		compatible = "regulator-fixed";
		regulator-name = "vdd_1v0";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <1000000>;
		regulator-max-microvolt = <1000000>;
	};

	vccio_sdio: vcc_1v8: vcc-1v8 {
		compatible = "regulator-fixed";
		regulator-name = "vcc_1v8";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
		vin-supply = <&vcc_io>;
	};

	vcc_sd: vcc-sd {
		compatible = "regulator-fixed";
		gpio = <&gpio4 RK_PD6 GPIO_ACTIVE_LOW>;
		regulator-name = "vcc_sd";
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	vcc_ddr: vcc-ddr {
		compatible = "regulator-fixed";
		regulator-name = "vcc_ddr";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <1800000>;
		regulator-max-microvolt = <1800000>;
	};

	vcc_io: vcc-io {
		compatible = "regulator-fixed";
		regulator-name = "vcc_io";
		regulator-always-on;
		regulator-boot-on;
		regulator-min-microvolt = <3300000>;
		regulator-max-microvolt = <3300000>;
	};

	vbus_host: vbus-host-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio0 RK_PC5 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&usb_drv>;
		regulator-name = "vbus_host";
	};

	wireless-bluetooth {
		compatible = "bluetooth-platdata";
		uart_rts_gpios = <&gpio4 RK_PA7 GPIO_ACTIVE_LOW>;
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart4_rts>;
		pinctrl-1 = <&uart4_rts_gpio>;
		BT,power_gpio    = <&gpio4 RK_PB3 GPIO_ACTIVE_HIGH>;
		BT,wake_host_irq = <&gpio4 RK_PB4 GPIO_ACTIVE_HIGH>;
		status = "okay";
	};

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		clocks = <&cru SCLK_WIFI>;
		clock-names = "clk_wifi";
		ext_clk_freq = <24000000>;
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_wake_host>;
		wifi_chip_type = "rtl8723ds";
		WIFI,host_wake_irq = <&gpio0 RK_PA0 GPIO_ACTIVE_LOW>;
		status = "okay";
	};
};

&acodec {
	status = "okay";

	rockchip,no-hp-det;
	rockchip,loopback-grp = <1>;
};

&cpu0 {
	cpu-supply = <&vdd_core>;
};

&cpu0_opp_table{
	opp-408000000 {
		opp-hz = /bits/ 64 <408000000>;
		opp-microvolt = <975000 975000 1340000>;
		clock-latency-ns = <40000>;
		opp-suspend;
	};
	opp-600000000 {
		opp-hz = /bits/ 64 <600000000>;
		opp-microvolt = <975000 975000 1340000>;
		clock-latency-ns = <40000>;
	};
};

&fiq_debugger {
	status = "okay";
};

&io_domains {
	status = "okay";

	vccio0-supply = <&vcc_io>;
	vccio1-supply = <&vcc_1v8>;
	vccio2-supply = <&vcc_1v8>;
	vccio3-supply = <&vcc_io>;
	vccio4-supply = <&vccio_sdio>;
	vccio5-supply = <&vcc_io>;
};

&i2s_8ch_2 {
	status = "okay";
};

&i2s_8ch_0 {
	status = "disabled";
    rockchip,clk-trcm=<1>;
};

&i2s_8ch_1 {
    status = "okay";
    pinctrl-names = "default";
    pinctrl-0 = <&i2s_8ch_1_m0_sclktx
        &i2s_8ch_1_m0_sclkrx
        &i2s_8ch_1_m0_lrcktx
        &i2s_8ch_1_m0_lrckrx
        &i2s_8ch_1_m0_sdi0
        &i2s_8ch_1_m0_sdo3_sdi1
        &i2s_8ch_1_m0_sdo2_sdi2
        &i2s_8ch_1_m0_sdo1_sdi3
        &i2s_8ch_1_m0_sdo0
        &i2s_8ch_1_m0_mclk>;
};

&nandc {
	status = "okay";
};

&ramoops {
	reg = <0x0 0x30000 0x0 0x20000>;
	console-size = <0x20000>;
	record-size = <0x00000>;
};

&saradc {
	status = "okay";
	vref-supply = <&vcc_1v8>;
};

&sdio {
	max-frequency = <110000000>;
	bus-width = <4>;
	cap-sd-highspeed;
	supports-sdio;
	ignore-pm-notify;
	keep-power-in-suspend;
	non-removable;
	mmc-pwrseq = <&sdio_pwrseq>;
	sd-uhs-sdr104;
	status = "okay";
};

&sdmmc {
	bus-width = <4>;
	cap-mmc-highspeed;
	cap-sd-highspeed;
	supports-sd;
	card-detect-delay = <300>;
	vmmc-supply = <&vcc_sd>;
	status = "okay";
};

&sfc {
	status = "okay";
};

&tsadc {
	rockchip,hw-tshut-mode = <0>; /* tshut mode 0:CRU 1:GPIO */
	rockchip,hw-tshut-polarity = <1>; /* tshut polarity 0:LOW 1:HIGH */
	status = "okay";
};

&pinctrl {
	pinctrl-names = "default";
	pinctrl-0 = <&rtc_32k>;

	buttons {
		volumeup: volumeup {
			rockchip,pins = <0 RK_PA7 RK_FUNC_GPIO &pcfg_pull_down>;
		};
		volumedown: volumedown {
			rockchip,pins = <0 RK_PA6 RK_FUNC_GPIO &pcfg_pull_down>;
		};
		mute: mute {
			rockchip,pins = <0 RK_PC4 RK_FUNC_GPIO &pcfg_pull_down>;
		};
	};

	usb {
		usb_drv: usb-drv {
			rockchip,pins = <0 RK_PC5 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins = <0 RK_PA2 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wireless-wlan {
		wifi_wake_host: wifi-wake-host {
			rockchip,pins = <0 RK_PA0 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
};

&pwm0 {
	status = "okay";
	pinctrl-names = "active";
	pinctrl-0 = <&pwm0_pin_pull_down>;
};



&u2phy {
	status = "okay";

	u2phy_host: host-port {
		phy-supply = <&vbus_host>;
		status = "okay";
	};

	u2phy_otg: otg-port {
		status = "okay";
	};
};

&uart4 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart4_xfer &uart4_cts>;
	status = "okay";
};

&uart2 {
	status = "disabled";
};

&usb20_otg {
	status = "okay";
};

&usb_host0_ehci {
	status = "okay";
};

&usb_host0_ohci{
	status = "okay";
};

&i2c0 {
    status="okay";
    clock-frequency = <400000>;

ac108: ac108@3b {
           status = "disabled";
           compatible = "MicArray_0";
           reg = <0x3b>;
           rockchip,grf = <&grf>;
           clocks = <&cru SCLK_I2S0_8CH_RX_OUT>;
           clock-names = "mclk_rx";
       };

tlv320dac3100: tlv320dac3100@18 {
                   status = "disabled";
                   compatible = "ti,tlv320dac3100";
                   reset-gpios = <&gpio1 RK_PC6 GPIO_ACTIVE_LOW>;
                   reg = <0x18>;
				   rockchip,grf = <&grf1>;
               };

cs35l41: cs35l41@40 {
             status="okay";
             #sound-dai-cells = <1>;
             compatible = "cirrus,cs35l41";
             reg = <0x40>;
             reset-gpios = <&gpio0 RK_PB6 GPIO_ACTIVE_HIGH>;
			 cirrus,irq-gpio = <&gpio0 RK_PB7 GPIO_ACTIVE_HIGH>;
             cirrus,boost-peak-milliamp = <4500>;
             cirrus,boost-ind-nanohenry = <1000>;
             cirrus,boost-cap-microfarad = <15>;

             cirrus,gpio-config2 {
                 cirrus,gpio-src-select = <0x4>;
                 cirrus,gpio-output-enable;
             };
         };
};

&i2c1 {
    status = "disabled";
	sn3106drv:sn3236drv@54 {
              status = "disabled";
              compatible = "rockchip,sn3106b";
              reg = <0x54>;
              gpios = <&gpio0 RK_PB0 GPIO_ACTIVE_HIGH>;
          };

};
